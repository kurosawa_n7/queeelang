# Queee

Queee is an esoteric programming language based on FIFO queue.

## Instructions

Queee has eigit instructions, `+`, `-`, `^`, `.`, `,`, `*`, `/`, and `!`.

* The command `+` pushes one into the queue.
* The command `-` pushes -1 into the queue.
* The command `^` pulls two elements (e.g., A and B) from the queue, and pushes the sum of the two (A+B).
* The command `,` receives the character from the standard input, and pushes the character code into the queue.
* The command `.` pulls one element from the queue and prints its corresponding character.
* The command `*` pulls one element from the queue and pushes it twice.
* The command `/` pulls one element from the queue and drop it.
* The command `!` pulls two elements (e.g., A and B) from the queue, and if A does not equal 0, B is add to the program counter.

Another character is treated as NOP.


## Note
* If the program counter P is larger than the program length or negative, the program terminates.

## Usage

````
queeelang <queeelang-program-path> 
````

