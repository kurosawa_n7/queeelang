module Main where

import System.IO(getChar, putChar, readFile)
import System.Environment(getArgs, getProgName)
import qualified Data.Sequence as S
import Data.Char(ord, chr)

type Program = S.Seq Char
type Memory = S.Seq Integer
type PC = Int

type Status = (Program, PC, Memory)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [f] -> do
      s <- readFile f
      (_,_,m) <- interpret (S.fromList s, 0, S.empty)
      print m
    _   -> usage

usage = do
  name <- getProgName
  putStrLn $ "usage: " ++ name ++ " program-file"


push s x = s S.|> x

pull s = case S.viewl s of
  S.EmptyL  -> error "no values in the queue"
  x S.:< ss -> (x, ss)

pull2 s0 = case S.viewl s0 of
  S.EmptyL  -> error "no values in the queue"
  x S.:< s1 -> case S.viewl s1 of
    S.EmptyL  -> error "no values in the queue"
    y S.:< s2 -> (x, y, s2)


interpret :: Status -> IO Status
interpret status@(prog, pc, m)
  | pc >= S.length prog  = return status
  | otherwise            = case (S.index prog pc) of
    '+' -> interpret (prog, pc+1, push m 1)
    '-' -> interpret (prog, pc+1, push m (-1))
    '^' -> let (x,y,m') = pull2 m in interpret (prog, pc+1, push m' (x+y)) 
    ',' -> getChar >>= \c -> interpret (prog, pc+1, push m (fromIntegral $ ord c))
    '.' -> let (x,m') = pull m in putChar (chr $ fromInteger x) >> interpret (prog, pc+1, m')
    '*' -> let (x,m') = pull m in interpret (prog, pc+1, push (push m' x) x)
    '/' -> let (_,m') = pull m in interpret (prog, pc+1, m')
    '!' -> let (x,y,m') = pull2 m in interpret (prog, pc+(if x == 0 then 0 else fromIntegral y), m')
    _   -> interpret (prog, pc+1, m)

